export default {
    game: {
        width: 480,
        height: 270,
        skipSplashState: false,
        skipMenuState: false,
        pixelArtRendering: true
    }
};