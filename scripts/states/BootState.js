export default class BootState extends Phaser.State
{
    preload()
    {
        this.game.stage.backgroundColor = '#EFE25C';
    }
	create()
    {
        this.game.state.start('LoadingState');
	}
}