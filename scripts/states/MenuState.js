export default class MenuState extends Phaser.State
{
    preload()
    {
        this.game.stage.backgroundColor = '#DD6918';
    }
	create()
    {
        this.game.state.start('GameState');
	}
}