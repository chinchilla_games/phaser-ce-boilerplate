import config from 'config.js';

export default class LoadingState extends Phaser.State
{
    preload()
    {
        this.game.load.image('chinchilla_games_logo', 'assets/images/chinchilla_games_logo.png');
        this.game.stage.backgroundColor = '#CC7EEF';
    }
	create()
    {
        this.game.state.start(
            config.game.skipSplashState
                ? (config.game.skipMenuState ? 'GameState' : 'MenuState')
                : 'SplashState'
        );
	}
}