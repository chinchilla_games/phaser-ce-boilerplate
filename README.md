# Phaser CE Boilerplate

Niniejsze repozytorium zawiera szkielet gry napisany przy pomocy biblioteki [Phaser CE](https://github.com/photonstorm/phaser-ce).

## Instalacja

Po pobraniu niniejszego repozytorium kodu należy zainstalować zależności (potrzebne biblioteki) przy użyciu narzędzia _npm_:
```
npm install
```
Powyższa komenda pobierze niezbędne biblioteki w tym silnik gry _Phaser CE_ w wersji 2.13.3.

## Opis struktury plików
```
.
├── build                   # Zbudowana wersja gry
├── scripts                 # Pliki źródłowe gry
│   ├── sprites             # Folder przeznaczony dla klas sprite-ów
│   ├── states              # Folder przeznaczony dla klas stanów gry
│   ├── config.js           # Konfiguracja gry
│   └── game.js             # Definiuje obiekt Phaser.Game
├── scss                    # Plik źródłowy dla arkusza stylów
│   └── game.scss           # Arkusz stylów gry
├── static                  # Zawiera statyczne źródła gry tj. obrazy, dźwięki, mapy, itd.
│   ├── assets
│   │   ├── audio           # Dźwięki i muzyka
│   │   ├── css             # Dodatkowe arkusze stylów
│   │   ├── fonts           # Czcionki
│   │   ├── images          # Pliki graficzne
│   │   └── js              # Dodatkowe skrypty JavaScript
│   └── index.html          # Plik zawierający strukturę HTML potrzebną do uruchomienia gry
├── README.md
└── credits.txt             # Linki do użytych zasobów 
```
## Uruchomienie gry

Aby uruchomić grę, należy wpisać komendę:
```
npm run dev
```
Powyższa komenda powinna uruchomić grę w wersji **deweloperskiej** pod adresem: _localhost:3000_. Uruchomiona w ten sposób gra zostanie 
każdorazowo przebudowana i zrestartowana, jeżeli zmienią się jej pliki (jednakże dodanie nowych plików wymaga ponownego uruchomienia).

Aby uruchomić grę w wersji **produkcyjnej**, użyj komendy:
```
npm run prod
```

Wersja deweloperska zawiera pliki map dla źródeł oraz dodatkowe narzędzie do pomiarów osiągów gry. Wersja produkcyjna zawiera zminifikowane źródła gry.

## Budowanie gry

Aby udostępnić grę należy ją zbudować. Do budowania należy użyć jednej z dwóch poniższych komend:
```
npm build
npm build -- --production
```
Pierwsza komenda buduję grę w trybie deweloperskim, druga w produkcyjnym, różnice są identyczne, jak w przypadku uruchomienia gry. 
Zbudowana wersja gry znajduje się w katalogu _build_ i zawiera wszystkie niezbędne zasoby.

## Uruchomienie gry w trybie okienkowym

Aby uruchomić grę w trybie okienkowym, należy wpisać komendę:
```
npm run start
npm run start -- --production
```
Pierwsza komenda uruchamia grę w trybie deweloperskim, druga w produkcyjnym, różnice są identyczne, jak w przypadku standardowego uruchomienia gry.
