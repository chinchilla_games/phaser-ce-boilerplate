const del = require('del');
const gulp = require('gulp');
const path = require('path');
const argv = require('yargs').argv;
const gutil = require('gulp-util');
const source = require('vinyl-source-stream');
const buffer = require('gulp-buffer');
const uglify = require('gulp-uglify');
const gulpif = require('gulp-if');
const exorcist = require('exorcist');
const babelify = require('babelify');
const browserify = require('browserify');
const browserSync = require('browser-sync');
const sass = require('gulp-sass');
const replace = require('gulp-replace');
const exec = require('child_process').exec;

let keepFiles = false;
const isProduction = () => argv.production;

gulp.task('clean-build', gulp.series((done) => {
    if (keepFiles === false) {
        del(['./build/**/*.*']);
    } else {
        keepFiles = false;
    }
    done();
}));

gulp.task('copy-static', gulp.series('clean-build', () => {
    return gulp.src(['./static/**/*', '!./static/main.js']).pipe(gulp.dest('./build'));
}));

const compileSCSS = () => {
    return gulp.src('./scss/*.scss').pipe(sass()).pipe(gulp.dest('./build/assets/css'));
};
gulp.task('compile-scss', gulp.series('copy-static', compileSCSS));
gulp.task('fast-compile-scss', gulp.series(compileSCSS));

gulp.task('copy-phaser', gulp.series('compile-scss', () => {
    let files = ['phaser.min.js'];
    if (isProduction() == false) {
        files.push('phaser.map', 'phaser.js');
    }
    return gulp.src(files.map(file => './node_modules/phaser-ce/build/' + file)).pipe(gulp.dest('./build/assets/js'));
}));

const build = () => {
    return browserify({
            paths: [path.join(__dirname, 'scripts')],
            entries: './scripts/game.js',
            debug: true,
            transform: [
                [
                    babelify, {
                        presets: ['es2015']
                    }
                ]
            ]
        })
        .transform(babelify)
        .bundle()
        .pipe(gulpif(isProduction() == false, exorcist('./build/assets/js/game.map')))
        .pipe(source('game.js'))
        .pipe(buffer())
        .pipe(replace('{{ENV}}', isProduction() ? 'prod' : 'dev'))
        .pipe(gulpif(isProduction(), uglify()))
        .pipe(gulp.dest('./build/assets/js'));
};
gulp.task('build', gulp.series('copy-phaser', build));
gulp.task('fast-build', gulp.series(build));

gulp.task('release-build', gulp.series('build', () => {
    return gulp.src(['./release-scripts/main.js']).pipe(gulp.dest('./build'));
}));

gulp.task('start', gulp.series('release-build', () => {
    return exec(
        __dirname + '/node_modules/.bin/electron .'
    ).on('close', () => process.exit());
}));

gulp.task('watch-scripts', gulp.series('fast-build', (done) => {
    browserSync.reload();
    done();
}));
gulp.task('watch-static', gulp.series('copy-phaser', (done) => {
    browserSync.reload();
    done();
}));
gulp.task('watch-scss', gulp.series('fast-compile-scss', (done) => {
    browserSync.reload();
    done();
}));

gulp.task('serve', gulp.series('build', (done) => {
    browserSync({
        server: {
            baseDir: './build'
        },
        open: false
    });
    gulp.watch('./scripts/**/*.js', gulp.series('watch-scripts'));
    gulp.watch('./static/**/*', gulp.series('watch-static')).on('change', () => (keepFiles = true));
    gulp.watch('./scss/*.scss', gulp.series('watch-scss'));
    done();
}));

gulp.task('default', gulp.series('serve'));
